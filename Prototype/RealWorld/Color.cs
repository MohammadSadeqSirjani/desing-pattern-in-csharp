﻿using System;

namespace Prototype.RealWorld
{
    public class Color : ColorPrototype
    {
        private readonly int _red;
        private readonly int _green;
        private readonly int _blue;

        public Color(int red, int green, int blue)
        {
            _red = red;
            _green = green;
            _blue = blue;
        }

        public override ColorPrototype Clone()
        {
            Console.WriteLine($"Cloning color RGB: {_red,3},{_green,3},{_blue,3}");
            return (ColorPrototype)MemberwiseClone();
        }
    }
}
