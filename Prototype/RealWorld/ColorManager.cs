﻿using System.Collections.Generic;

namespace Prototype.RealWorld
{
    public class ColorManager
    {
        private readonly IDictionary<string, ColorPrototype>
            _colors = new Dictionary<string, ColorPrototype>();

        public ColorPrototype this[string key]
        {
            get => _colors[key];
            set => _colors[key] = value;
        }
    }
}
