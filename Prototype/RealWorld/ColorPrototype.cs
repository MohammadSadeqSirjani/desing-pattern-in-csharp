﻿namespace Prototype.RealWorld
{
    public abstract class ColorPrototype
    {

        public abstract ColorPrototype Clone();
    }
}
