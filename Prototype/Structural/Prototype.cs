﻿namespace Prototype.Structural
{
    public abstract class Prototype
    {
        private readonly string _id;

        public string Id => _id;

        public Prototype(string id)
        {
            _id = id;
        }

        public abstract Prototype Clone();
    }
}
