﻿using Prototype.RealWorld;
using Prototype.Structural;
using System;

namespace Prototype
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            RealWorld();
        }

        private static void RealWorld()
        {
            var colorManager = new ColorManager
            {
                ["red"] = new Color(255, 0, 0),
                ["green"] = new Color(0, 255, 0),
                ["blue"] = new Color(0, 0, 255),
                ["angry"] = new Color(255, 54, 0),
                ["peace"] = new Color(128, 211, 128),
                ["flame"] = new Color(211, 34, 20),
            };

            var color1 = colorManager["red"].Clone() as Color;
            var color2 = colorManager["peace"].Clone() as Color;
            var color3 = colorManager["fla"].Clone() as Color;
        }

        private static void Structural()
        {
            var concretePrototype1 = new ConcretePrototype1("I");
            var cloneConcretePrototype1 = (ConcretePrototype1)concretePrototype1.Clone();
            Console.WriteLine($"Cloned {cloneConcretePrototype1.Id}");

            var concretePrototype2 = new ConcretePrototype2("II");
            var cloneConcretePrototype2 = (ConcretePrototype2)concretePrototype2.Clone();
            Console.WriteLine($"Cloned {cloneConcretePrototype2.Id}");
        }
    }
}
