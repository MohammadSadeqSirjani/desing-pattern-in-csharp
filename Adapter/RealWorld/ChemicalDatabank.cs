﻿namespace Adapter.RealWorld
{
    public class ChemicalDatabank
    {
        public static float GetCriticalPoint(string compound, string point)
        {
            compound = compound.ToLower();
            return point == "M"
                ? compound switch
                {
                    "water" => 0.0f,
                    "benzene" => 5.5f,
                    "ethanol" => -114.1f,
                    _ => 0f
                }
                : compound switch
                {
                    "water" => 100.0f,
                    "benzene" => 80.1f,
                    "ethanol" => 78.3f,
                    _ => 0f
                };
        }

        public static string GetMolecularStructure(string compound)
        {
            return compound.ToLower() switch
            {
                "water" => "H2O",
                "benzene" => "C6H6",
                "ethanol" => "C2H5OH",
                _ => ""
            };
        }

        public static double GetMolecularWeight(string compound)
        {
            return compound.ToLower() switch
            {
                "water" => 18.015d,
                "benzene" => 78.1134d,
                "ethanol" => 46.0688d,
                _ => 0d
            };
        }
    }
}