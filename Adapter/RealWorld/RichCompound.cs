﻿using System;

namespace Adapter.RealWorld
{
    public class RichCompound : Compound
    {
        public RichCompound(string chemical) : base(chemical)
        {
        }

        public override void Display()
        {
            BoilingPoint = ChemicalDatabank.GetCriticalPoint(Chemical, "B");
            MeltingPoint = ChemicalDatabank.GetCriticalPoint(Chemical, "M");
            MolecularWeight = ChemicalDatabank.GetMolecularWeight(Chemical);
            MolecularFormula = ChemicalDatabank.GetMolecularStructure(Chemical);

            base.Display();
            Console.WriteLine($" Formula: {MolecularFormula}");
            Console.WriteLine($" Weight : {MolecularWeight}");
            Console.WriteLine($" Melting Point: {MeltingPoint}");
            Console.WriteLine($" Boiling Point: {BoilingPoint}");
        }
    }
}