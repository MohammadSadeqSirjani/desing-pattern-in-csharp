﻿using Adapter.RealWorld;

namespace Adapter
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var unknown = new Compound("Unknown");
            unknown.Display();

            var water = new RichCompound("Water");
            water.Display();

            var benzene = new RichCompound("Benzene");
            benzene.Display();

            var ethanol = new RichCompound("Ethanol");
            ethanol.Display();
        }

        private static void Structural()
        {
            var adapter = new Structural.Adapter();
            adapter.Request();
        }
    }
}
