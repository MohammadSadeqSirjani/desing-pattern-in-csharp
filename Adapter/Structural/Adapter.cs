﻿using System;

namespace Adapter.Structural
{
    public class Adapter : Target
    {
        private readonly Adaptee _adaptee = new Adaptee();

        public override void Request()
        {
            Console.WriteLine($"Called {nameof(Adapter)} - {nameof(Request)}");
            _adaptee.SpecificRequest();
        }
    }
}
