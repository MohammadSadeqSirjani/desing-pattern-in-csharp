﻿using System;

namespace Adapter.Structural
{
    public class Target
    {
        public virtual void Request() => Console.WriteLine($"Called {nameof(Target)} - {nameof(Request)}");
    }
}
