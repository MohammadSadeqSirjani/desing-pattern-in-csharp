﻿using System;

namespace Adapter.Structural
{
    public class Adaptee
    {
        public void SpecificRequest() => Console.WriteLine($"Called {nameof(Adaptee)} - {nameof(SpecificRequest)}");
    }
}
