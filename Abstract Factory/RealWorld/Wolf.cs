﻿using System;

namespace AbstractFactory.RealWorld
{
    public class Wolf : Carnivore
    {
        public override void Eat(Herbivore herbivore)
        {
            Console.WriteLine($"{this.GetType().Name} eats {herbivore.GetType().Name}.");
        }
    }
}