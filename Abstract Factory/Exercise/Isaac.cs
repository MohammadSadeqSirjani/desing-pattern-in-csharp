﻿using System;

namespace AbstractFactory.Exercise
{
    public class Isaac : Male
    {
        public override void Marry(Female female)
        {
            Console.WriteLine($"{GetType().Name} married with {female.GetType().Name}.");
        }
    }
}
