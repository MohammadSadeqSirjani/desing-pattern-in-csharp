﻿using System;

namespace AbstractFactory.Exercise
{
    class Mohammad : Male
    {
        public override void Marry(Female female)
        {
            Console.WriteLine($"{GetType().Name} married with {female.GetType().Name}.");
        }
    }
}
