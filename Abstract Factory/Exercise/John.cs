﻿using System;

namespace AbstractFactory.Exercise
{
    public class John : Male
    {
        public override void Marry(Female female)
        {
            Console.WriteLine($"{GetType().Name} married with {female.GetType().Name}.");
        }
    }
}
