﻿namespace AbstractFactory.Exercise
{
    public class ChristianMarriage : Marriage
    {
        public override Male CreateMale()
        {
            return new Isaac();
        }

        public override Female CreateFemale()
        {
            return new Mary();
        }
    }
}
