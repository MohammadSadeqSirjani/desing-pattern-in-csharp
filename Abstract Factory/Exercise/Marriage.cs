﻿namespace AbstractFactory.Exercise
{
    public abstract class Marriage
    {
        public abstract Male CreateMale();
        public abstract Female CreateFemale();
    }
}
