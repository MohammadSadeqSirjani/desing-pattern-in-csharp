﻿namespace AbstractFactory.Exercise
{
    public abstract class Male
    {
        public abstract void Marry(Female female);
    }
}
