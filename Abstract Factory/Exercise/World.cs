﻿namespace AbstractFactory.Exercise
{
    public class World
    {
        private readonly Male _male;
        private readonly Female _female;

        public World(Marriage marriage)
        {
            _male = marriage.CreateMale();
            _female = marriage.CreateFemale();
        }

        public void MakeLife()
        {
            _male.Marry(_female);
        }
    }
}
