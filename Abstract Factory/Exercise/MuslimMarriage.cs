﻿namespace AbstractFactory.Exercise
{
    public class MuslimMarriage : Marriage
    {
        public override Male CreateMale()
        {
            return new Mohammad();
        }

        public override Female CreateFemale()
        {
            return new Sara();
        }
    }
}
