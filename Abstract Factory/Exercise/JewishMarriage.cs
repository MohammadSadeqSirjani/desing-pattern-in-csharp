﻿namespace AbstractFactory.Exercise
{
    public class JewishMarriage : Marriage
    {
        public override Male CreateMale()
        {
            return new John();
        }

        public override Female CreateFemale()
        {
            return new Anna();
        }
    }
}