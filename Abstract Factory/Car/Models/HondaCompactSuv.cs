﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class HondaCompactSuv : ISuv, IMakeSuv
    {
        public string Name()
        {
            return "Honda CR-V";
        }

        public ISuv MakeSuv()
        {
            return this;
        }
    }
}
