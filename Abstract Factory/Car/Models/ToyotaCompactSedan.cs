﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class ToyotaCompactSedan : ISedan, IMakeSedan
    {
        public string Name()
        {
            return "Toyota Yaris";
        }

        public ISedan MakeSedan()
        {
            return this;
        }
    }
}
