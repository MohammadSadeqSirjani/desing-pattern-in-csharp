﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class HondaCompactSedan : ISedan, IMakeSedan
    {
        public string Name()
        {
            return "Honda Amaze";
        }

        public ISedan MakeSedan()
        {
            return this;
        }
    }
}