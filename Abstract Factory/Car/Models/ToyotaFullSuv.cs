﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class ToyotaFullSuv : ISuv, IMakeSuv
    {
        public string Name()
        {
            return "Toyota Highlander";
        }

        public ISuv MakeSuv()
        {
            return this;
        }
    }
}
