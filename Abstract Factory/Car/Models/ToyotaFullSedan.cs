﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class ToyotaFullSedan : ISedan, IMakeSedan
    {
        public string Name()
        {
            return "Toyota Camery";
        }

        public ISedan MakeSedan()
        {
            return this;
        }
    }
}
