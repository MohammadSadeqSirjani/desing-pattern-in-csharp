﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class HondaFullSuv : ISuv, IMakeSuv
    {
        public string Name()
        {
            return "Honda Pilot";
        }

        public ISuv MakeSuv()
        {
            return this;
        }
    }
}