﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class ToyotaCompactSuv : ISuv, IMakeSuv
    {
        public string Name()
        {
            return "Toyota Rav-4";
        }

        public ISuv MakeSuv()
        {
            return this;
        }
    }
}
