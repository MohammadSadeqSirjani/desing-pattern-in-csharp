﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Models
{
    public class HondaFullSedan : ISedan, IMakeSedan
    {
        public string Name()
        {
            return "Honda Accord";
        }

        public ISedan MakeSedan()
        {
            return this;
        }
    }
}