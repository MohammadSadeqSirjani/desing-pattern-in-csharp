﻿using AbstractFactory.Car.Factory.Interfaces;
using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car
{
    public class CarClient
    {
        private readonly ISedan _sedan;
        private readonly ISuv _suv;

        public CarClient(ICarFactory carFactory, IMakeSedan sedan, IMakeSuv suv)
        {
            _sedan = carFactory.ManufactureSedan(sedan);
            _suv = carFactory.ManufactureSuv(suv);
        }

        public string GetManufacturedSedanName()
        {
            return _sedan.Name();
        }

        public string GetManufacturedSuvName()
        {
            return _suv.Name();
        }
    }
}
