﻿using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Factory.Interfaces
{
    public interface ICarFactory
    {
        ISedan ManufactureSedan(IMakeSedan sedan);
        ISuv ManufactureSuv(IMakeSuv suv);
    }
}
