﻿using AbstractFactory.Car.Factory.Interfaces;
using AbstractFactory.Car.Interfaces;

namespace AbstractFactory.Car.Factory.Services
{
    public class ToyotaFactory : ICarFactory
    {
        public ISedan ManufactureSedan(IMakeSedan sedan)
        {
            return sedan.MakeSedan();
        }

        public ISuv ManufactureSuv(IMakeSuv suv)
        {
            return suv.MakeSuv();
        }
    }

}
