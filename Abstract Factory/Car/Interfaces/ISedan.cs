﻿namespace AbstractFactory.Car.Interfaces
{
    public interface ISedan
    {
        string Name();
    }
}
