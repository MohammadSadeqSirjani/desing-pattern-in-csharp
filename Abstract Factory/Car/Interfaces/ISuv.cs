﻿namespace AbstractFactory.Car.Interfaces
{
    public interface ISuv
    {
        string Name();
    }
}
