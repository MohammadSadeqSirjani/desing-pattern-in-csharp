﻿namespace AbstractFactory.Car.Interfaces
{
    public interface IMakeSuv
    {
        ISuv MakeSuv();
    }
}