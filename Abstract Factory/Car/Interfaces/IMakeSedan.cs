﻿namespace AbstractFactory.Car.Interfaces
{
    public interface IMakeSedan
    {
        ISedan MakeSedan();
    }
}
