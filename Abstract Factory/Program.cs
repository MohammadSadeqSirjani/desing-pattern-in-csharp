﻿using AbstractFactory.Car;
using AbstractFactory.Car.Factory.Services;
using AbstractFactory.Car.Models;
using AbstractFactory.Exercise;
using AbstractFactory.RealWorld;
using AbstractFactory.Structural;
using System;

namespace AbstractFactory
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Car();
        }

        private static void Car()
        {
            Console.WriteLine("\r\n------------This is HONDA Car Factory----------------");
            var hondaClient = new CarClient(new HondaFactory(), new HondaCompactSedan(), new HondaCompactSuv());
            Console.WriteLine("\r\n Manufacturing " + hondaClient.GetManufacturedSedanName() + " as compact Sedan");
            Console.WriteLine("\r\n Manufacturing " + hondaClient.GetManufacturedSuvName() + " as compact SUV");

            hondaClient = new CarClient(new HondaFactory(), new HondaFullSedan(), new HondaFullSuv());
            Console.WriteLine("\r\n Manufacturing " + hondaClient.GetManufacturedSedanName() + " as full Sedan");
            Console.WriteLine("\r\n Manufacturing " + hondaClient.GetManufacturedSuvName() + " as full SUV");

            Console.WriteLine("\r\n\r\n------------This is TOYOTA Car Factory----------------");
            var toyotaClient = new CarClient(new ToyotaFactory(), new ToyotaCompactSedan(), new ToyotaCompactSuv());
            Console.WriteLine("\r\n Manufacturing " + toyotaClient.GetManufacturedSedanName() + " as compact Sedan");
            Console.WriteLine("\r\n Manufacturing " + toyotaClient.GetManufacturedSuvName() + " as compact SUV");

            toyotaClient = new CarClient(new ToyotaFactory(), new ToyotaFullSedan(), new ToyotaFullSuv());
            Console.WriteLine("\r\n Manufacturing " + toyotaClient.GetManufacturedSedanName() + " as full Sedan");
            Console.WriteLine("\r\n Manufacturing " + toyotaClient.GetManufacturedSuvName() + " as full SUV");
            Console.ReadLine();
        }

        private static void Exercise()
        {
            Marriage muslim = new MuslimMarriage();
            var muslimWorld = new World(muslim);
            muslimWorld.MakeLife();

            Marriage christian = new ChristianMarriage();
            var christianWorld = new World(christian);
            christianWorld.MakeLife();

            Marriage jewish = new JewishMarriage();
            var jewishWorld = new World(jewish);
            jewishWorld.MakeLife();
        }

        private static void RealWorld()
        {
            ContinentFactory africa = new AfricaFactory();
            var africaWorld = new AnimalWorld(africa);
            africaWorld.RunFoodChain();

            ContinentFactory america = new AmericaFactory();
            var americaWorld = new AnimalWorld(america);
            americaWorld.RunFoodChain();
        }

        private static void Structural()
        {
            AbstractFactory.Structural.AbstractFactory factory1 = new ConcreteFactory1();

            var client1 = new Client(factory1);
            client1.Run();

            AbstractFactory.Structural.AbstractFactory factory2 = new ConcreteFactory2();

            var client2 = new Client(factory2);
            client2.Run();
        }
    }
}
