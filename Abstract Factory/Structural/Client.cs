﻿namespace AbstractFactory.Structural
{
    public class Client
    {
        private readonly AbstractProductA _abstractProductA;
        private readonly AbstractProductB _abstractProductB;

        public Client(global::AbstractFactory.Structural.AbstractFactory abstractFactory)
        {
            _abstractProductA = abstractFactory.CreateProductA();
            _abstractProductB = abstractFactory.CreateProductB();
        }

        public void Run()
        {
            _abstractProductB.Intersect(_abstractProductA);
        }
    }
}