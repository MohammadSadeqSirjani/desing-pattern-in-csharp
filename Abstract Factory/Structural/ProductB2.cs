﻿using System;

namespace AbstractFactory.Structural
{
    public class ProductB2 : AbstractProductB
    {
        public override void Intersect(AbstractProductA abstractProductA)
        {
            Console.WriteLine($"{this.GetType().Name} intersect with {abstractProductA.GetType().Name}.");
        }
    }
}