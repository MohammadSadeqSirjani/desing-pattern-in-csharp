﻿namespace AbstractFactory.Structural
{
    public abstract class AbstractProductB
    {
        public abstract void Intersect(AbstractProductA abstractProductA);
    }
}