﻿using System;

namespace AbstractFactory.Structural
{
    public class ProductB1 : AbstractProductB
    {
        public override void Intersect(AbstractProductA abstractProductA)
        {
            Console.WriteLine($"{this.GetType().Name} intersect with {abstractProductA.GetType().Name}.");
        }
    }
}