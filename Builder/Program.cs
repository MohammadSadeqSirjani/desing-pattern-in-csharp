﻿using Builder.Exercise;
using Builder.Pizza;
using Builder.RealWorld;
using Builder.Structural;
using System;

namespace Builder
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            MakePizza();
        }

        private static void MakePizza()
        {
            var pizza = new PizzaBuilder()
                .AddHam()
                .AddMushrooms()
                .AddOlives()
                .ChooseCheese(Cheese.Cheddar)
                .Cook();

            Console.WriteLine(pizza);
        }

        private static void Exercise()
        {
            var drinkMaker = new DrinkMaker();
            drinkMaker.MakeDrink(new TeaBuilder());
            drinkMaker.MakeDrink(new CoffeeBuilder());
        }

        private static void RealWorld()
        {
            var shop = new Shop();

            VehicleBuilder vehicleBuilder = new MotorCycleBuilder();
            shop.Construct(vehicleBuilder);
            vehicleBuilder.Vehicle.Show();

            vehicleBuilder = new CarBuilder();
            shop.Construct(vehicleBuilder);
            vehicleBuilder.Vehicle.Show();

            vehicleBuilder = new ScooterBuilder();
            shop.Construct(vehicleBuilder);
            vehicleBuilder.Vehicle.Show();
        }

        private static void Structural()
        {
            var director = new Director();

            var builder1 = new ConcreteBuilder1();
            director.Construct(builder1);
            var product1 = builder1.GetResult();
            product1.Show();


            var builder2 = new ConcreteBuilder2();
            director.Construct(builder2);
            var product2 = builder2.GetResult();
            product2.Show();
        }
    }
}
