﻿namespace Builder.Pizza
{
    public class PizzaBuilder : IPizzaBuilder
    {
        public Size Size { get; private set; } = Size.Large;
        public Cheese Cheese { get; private set; } = Cheese.Mozzarella;
        public bool Sauce { get; private set; }
        public bool Pepperoni { get; private set; }
        public bool Ham { get; private set; }
        public bool Olives { get; private set; }
        public bool Mushrooms { get; private set; }

        public PizzaBuilder ChooseSize(Size size = Size.Large)
        {
            Size = size;
            return this;
        }

        public PizzaBuilder ChooseCheese(Cheese cheese = Cheese.Mozzarella)
        {
            Cheese = cheese;
            return this;
        }

        public PizzaBuilder AddSauce()
        {
            Sauce = true;
            return this;
        }

        public PizzaBuilder AddPepperoni()
        {
            Pepperoni = true;
            return this;
        }

        public PizzaBuilder AddHam()
        {
            Ham = true;
            return this;
        }

        public PizzaBuilder AddOlives()
        {
            Olives = true;
            return this;
        }

        public PizzaBuilder AddMushrooms()
        {
            Mushrooms = true;
            return this;
        }

        public Pizza Cook()
        {
            return new Pizza(this);
        }
    }
}
