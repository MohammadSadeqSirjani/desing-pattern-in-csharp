﻿namespace Builder.Pizza
{
    public enum Cheese
    {
        Cheddar,
        Mozzarella,
        Parmesan,
        Provolone
    }
}