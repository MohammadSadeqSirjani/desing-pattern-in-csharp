﻿namespace Builder.Pizza
{
    public class Pizza
    {
        private readonly Size _size;
        private readonly Cheese _cheese;
        private readonly bool _sauce;
        private readonly bool _pepperoni;
        private readonly bool _ham;
        private readonly bool _olives;
        private readonly bool _mushrooms;

        public Pizza(IPizzaBuilder pizzaBuilder)
        {
            _size = pizzaBuilder.Size;
            _cheese = pizzaBuilder.Cheese;
            _sauce = pizzaBuilder.Sauce;
            _pepperoni = pizzaBuilder.Pepperoni;
            _ham = pizzaBuilder.Ham;
            _olives = pizzaBuilder.Olives;
            _mushrooms = pizzaBuilder.Mushrooms;
        }


        public override string ToString()
        {
            return
                $"Size : {_size}\n" +
                $"Cheese : {_cheese}\n" +
                $"Sauce : {_sauce}\n" +
                $"Pepperoni : {_pepperoni}\n" +
                $"Ham : {_ham}\n" +
                $"Olives : {_olives}\n" +
                $"Mushroom : {_mushrooms}";
        }
    }
}
