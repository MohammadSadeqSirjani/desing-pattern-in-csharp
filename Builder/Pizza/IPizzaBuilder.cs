﻿namespace Builder.Pizza
{
    public interface IPizzaBuilder
    {
        Size Size { get; }
        Cheese Cheese { get; }
        bool Sauce { get; }
        bool Pepperoni { get; }
        bool Ham { get; }
        bool Olives { get; }
        bool Mushrooms { get; }

        PizzaBuilder ChooseSize(Size size);
        PizzaBuilder ChooseCheese(Cheese cheese);
        PizzaBuilder AddSauce();
        PizzaBuilder AddPepperoni();
        PizzaBuilder AddHam();
        PizzaBuilder AddOlives();
        PizzaBuilder AddMushrooms();
        Pizza Cook();
    }
}
