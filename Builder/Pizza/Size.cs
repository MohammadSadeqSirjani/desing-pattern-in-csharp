﻿namespace Builder.Pizza
{
    public enum Size
    {
        Small,
        Medium,
        Large,
        Super
    }
}