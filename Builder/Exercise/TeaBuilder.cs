﻿using System;

namespace Builder.Exercise
{
    public class TeaBuilder : IDrinkBuilder
    {
        public void AddPowder()
        {
            Console.WriteLine($"Add powder to {this.GetType().Name}.");
        }

        public void AddWater()
        {
            Console.WriteLine($"Add water to {this.GetType().Name}.");
        }

        public Drink Drink { get; }
    }
}
