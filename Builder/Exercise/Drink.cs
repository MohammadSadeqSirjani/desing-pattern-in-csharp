﻿namespace Builder.Exercise
{
    public class Drink
    {
        private readonly string _label;

        public Drink(string label)
        {
            _label = label;
        }

        public override string ToString()
        {
            return _label;
        }
    }
}
