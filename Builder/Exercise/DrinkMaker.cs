﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Exercise
{
    public class DrinkMaker
    {
        public void MakeDrink(IDrinkBuilder drinkBuilder)
        {
            drinkBuilder.AddPowder();
            drinkBuilder.AddWater();
            Console.WriteLine(new string('-', 30));
        }
    }
}
