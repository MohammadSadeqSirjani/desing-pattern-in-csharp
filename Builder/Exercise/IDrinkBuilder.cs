﻿namespace Builder.Exercise
{
    public interface IDrinkBuilder
    {
        void AddPowder();
        void AddWater();
        Drink Drink { get; }
    }
}
