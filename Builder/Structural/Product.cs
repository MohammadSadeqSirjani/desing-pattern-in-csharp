﻿using System;
using System.Collections.Generic;

namespace Builder.Structural
{
    public class Product
    {
        private readonly IList<string> _paths = new List<string>();

        public void Add(string path) => _paths.Add(path);
        public void Show()
        {
            Console.WriteLine("\nProduct Parts ...");
            foreach (var path in _paths) Console.WriteLine(path);
        }
    }
}