﻿namespace Builder.Structural
{
    public class Director
    {
        public void Construct(Structural.Builder builder)
        {
            builder.BuildPartA();
            builder.BuildPartB();
        }
    }
}