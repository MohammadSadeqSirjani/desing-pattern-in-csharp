﻿namespace Builder.Structural
{
    public class ConcreteBuilder2 : Structural.Builder
    {
        private readonly Product _product = new Product();

        public override void BuildPartA()
        {
            _product.Add("PartA2");
        }

        public override void BuildPartB()
        {
            _product.Add("PartB2");
        }

        public override Product GetResult()
        {
            return _product;
        }
    }
}