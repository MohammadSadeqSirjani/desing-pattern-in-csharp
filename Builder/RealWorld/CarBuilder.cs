﻿namespace Builder.RealWorld
{
    public class CarBuilder : VehicleBuilder
    {
        public CarBuilder()
        {
            InternalVehicle = new Vehicle("Car");
        }

        public override void BuildFrame()
        {
            InternalVehicle["Frame"] = "Car Frame";
        }

        public override void BuildEngine()
        {
            InternalVehicle["Engine"] = "2500 cc";
        }

        public override void BuildWheels()
        {
            InternalVehicle["Wheels"] = "4";
        }

        public override void BuildDoors()
        {
            InternalVehicle["Doors"] = "4";
        }
    }
}
