﻿using System;
using System.Collections.Generic;

namespace Builder.RealWorld
{
    public class Vehicle
    {
        private readonly string _vehicleType;
        private readonly IDictionary<string, string> _parts = new Dictionary<string, string>();

        public Vehicle(string vehicleType)
        {
            _vehicleType = vehicleType;
        }

        public string this[string key]
        {
            get => _parts[key];
            set => _parts[key] = value;
        }

        public void Show()
        {
            Console.WriteLine("\n---------------------------");
            Console.WriteLine($" #Vehicle Type: {_vehicleType}");
            Console.WriteLine($" #Frame : {_parts["Frame"] }");
            Console.WriteLine($" #Engine : {_parts["Engine"]}");
            Console.WriteLine($" #Wheels: {_parts["Wheels"]}");
            Console.WriteLine($" #Doors : {_parts["Doors"]}");
        }
    }
}
