﻿namespace Builder.RealWorld
{
    public class MotorCycleBuilder : VehicleBuilder
    {
        public MotorCycleBuilder()
        {
            InternalVehicle = new Vehicle("MotorCycle");
        }

        public override void BuildFrame()
        {
            InternalVehicle["Frame"] = "MotorCycle Frame";
        }

        public override void BuildEngine()
        {
            InternalVehicle["Engine"] = "500 cc";
        }

        public override void BuildWheels()
        {
            InternalVehicle["Wheels"] = "2";
        }

        public override void BuildDoors()
        {
            InternalVehicle["Doors"] = "0";
        }
    }
}
