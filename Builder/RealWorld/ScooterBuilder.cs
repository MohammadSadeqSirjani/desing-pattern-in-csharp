﻿namespace Builder.RealWorld
{
    public class ScooterBuilder : VehicleBuilder
    {
        public ScooterBuilder()
        {
            InternalVehicle = new Vehicle("Scooter");
        }

        public override void BuildFrame()
        {
            InternalVehicle["Frame"] = "Scooter Frame";
        }

        public override void BuildEngine()
        {
            InternalVehicle["Engine"] = "50 cc";
        }

        public override void BuildWheels()
        {
            InternalVehicle["Wheels"] = "2";
        }

        public override void BuildDoors()
        {
            InternalVehicle["Doors"] = "0";
        }
    }
}
