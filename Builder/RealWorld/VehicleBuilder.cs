﻿namespace Builder.RealWorld
{
    public abstract class VehicleBuilder
    {
        protected Vehicle InternalVehicle { get; set; }

        public Vehicle Vehicle => InternalVehicle;

        public abstract void BuildFrame();
        public abstract void BuildEngine();
        public abstract void BuildWheels();
        public abstract void BuildDoors();
    }
}
