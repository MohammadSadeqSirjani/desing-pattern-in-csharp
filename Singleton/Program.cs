﻿using Singleton.Exercise;
using Singleton.RealWorld;
using System;

namespace Singleton
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Exercise();
        }

        private static void Exercise()
        {
            var a = Number.GetInstance();
            var b = Number.GetInstance();

            a.Increment();
            a.Increment();
            a.Increment();
            a.Increment();
            b.Decrement();

            a.Print();

            a.Decrement();

            b.Print();
        }

        private static void RealWorld()
        {
            var b1 = LoadBalancer.GetLoadBalancer();
            var b2 = LoadBalancer.GetLoadBalancer();
            var b3 = LoadBalancer.GetLoadBalancer();
            var b4 = LoadBalancer.GetLoadBalancer();

            if (b1 == b2 && b2 == b3 && b3 == b4) Console.WriteLine("Same instance.\n");

            var balancer = LoadBalancer.GetLoadBalancer();
            for (var i = 0; i < 15; i++)
            {
                var server = balancer.NexServer;
                Console.WriteLine($"Dispatch Request to {server.Name} - {server.Ip}");
            }
        }

        private static void Structural()
        {
            var a = Singleton.Structural.Singleton.Instance();
            var b = Singleton.Structural.Singleton.Instance();

            if (a == b) Console.WriteLine("B: Objects are the same.");
        }
    }
}
