﻿using System;

namespace Singleton.Exercise
{
    public class Number
    {
        private static readonly Number Instance = new Number();
        private int _counter = 0;

        private Number()
        {

        }

        public static Number GetInstance() => Instance;

        public int Increment() => _counter++;
        public int Decrement() => _counter--;

        public void Print() => Console.WriteLine($"Counter Value: {_counter}");
    }
}
