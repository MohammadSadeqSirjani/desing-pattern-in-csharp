﻿namespace Singleton.Structural
{
    public class Singleton
    {
        private static Singleton _instance;

        private Singleton()
        {

        }

        /*
         * Uses lazy initialization
         * Note: this is not thread safe
         */
        public static Singleton Instance() => _instance ??= new Singleton();
    }
}
