﻿namespace Singleton.RealWorld
{
    public class Server
    {
        public string Name { get; set; }

        public string Ip { get; set; }
    }
}