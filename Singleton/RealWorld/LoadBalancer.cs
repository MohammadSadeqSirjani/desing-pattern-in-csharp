﻿using System;
using System.Collections.Generic;

namespace Singleton.RealWorld
{
    public class LoadBalancer
    {
        private static readonly LoadBalancer Instance = new LoadBalancer();

        private IList<Server> Servers { get; set; }
        private readonly Random _random = new Random();

        private LoadBalancer()
        {
            Servers = new List<Server>()
            {
                new Server() {Name = "Server I", Ip = "120.14.220.18"},
                new Server() {Name = "Server II", Ip = "120.14.220.19"},
                new Server() {Name = "Server III", Ip = "120.14.220.20"},
                new Server() {Name = "Server IV", Ip = "120.14.220.21"},
                new Server() {Name = "Server V", Ip = "120.14.220.22"}
            };
        }

        public static LoadBalancer GetLoadBalancer() => Instance;

        public Server NexServer => Servers[_random.Next(Servers.Count)];
    }
}
