﻿using FactoryMethod.Exercise;
using FactoryMethod.RealWorld;
using FactoryMethod.Structural;
using System;

namespace FactoryMethod
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Exercise();
        }

        private static void Exercise()
        {
            var shops = new IShop[2];

            shops[0] = new FairCar();
            shops[1] = new MallCar();

            foreach (var shop in shops)
            {
                var models = shop.PurchaseCar();
                foreach (var model in models)
                {
                    model.GetCarModel();
                    Console.WriteLine($"{model.GetType().Name} is {model.CarColor}");
                }
            }
        }

        private static void RealWorld()
        {
            var documents = new Document[2];

            documents[0] = new Resume();
            documents[1] = new Report();

            foreach (var document in documents)
            {
                Console.WriteLine($"\n{document.GetType().Name} --");
                foreach (var page in document.Pages) Console.WriteLine($" {page.GetType().Name}");
            }
        }

        private static void Structural()
        {
            var creators = new Creator[2];

            creators[0] = new ConcreteCreatorA();
            creators[1] = new ConcreteCreatorB();

            foreach (var creator in creators)
            {
                var product = creator.FactoryMethod();
                Console.WriteLine($"Created {product.GetType().Name}");
            }
        }
    }
}
