﻿namespace FactoryMethod.RealWorld
{
    public class Resume : Document
    {
        public override void CreatePages()
        {
            Pages.Add(new SkillsPage());
            Pages.Add(new EducationalPage());
            Pages.Add(new ExperiencePage());
        }
    }
}