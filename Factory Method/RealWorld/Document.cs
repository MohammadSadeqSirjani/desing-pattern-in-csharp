﻿using System.Collections.Generic;

namespace FactoryMethod.RealWorld
{
    public abstract class Document
    {
        private readonly IList<Page> _pages = new List<Page>();

        public IList<Page> Pages => _pages;

        public Document()
        {
            CreatePages();
        }

        public abstract void CreatePages();
    }
}
