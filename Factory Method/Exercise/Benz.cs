﻿using System;

namespace FactoryMethod.Exercise
{
    public class Benz : ICarSupplier
    {
        public string CarColor => "Carbon";
        public void GetCarModel()
        {
            Console.WriteLine("Benz car model is Honda 2020.");
        }
    }
}