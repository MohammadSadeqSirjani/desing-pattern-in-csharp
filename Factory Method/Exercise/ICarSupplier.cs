﻿namespace FactoryMethod.Exercise
{
    public interface ICarSupplier
    {
        string CarColor
        {
            get;
        }

        void GetCarModel();
    }
}
