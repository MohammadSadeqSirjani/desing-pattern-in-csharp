﻿using System.Collections.Generic;

namespace FactoryMethod.Exercise
{
    public class FairCar : IShop
    {
        public IList<ICarSupplier> PurchaseCar()
        {
            return new List<ICarSupplier>()
            {
                new Benz(),
                new Bmw(),
                new Honda(),
                new Toyota()
            };
        }
    }
}
