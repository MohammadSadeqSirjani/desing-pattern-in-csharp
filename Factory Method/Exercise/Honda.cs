﻿using System;

namespace FactoryMethod.Exercise
{
    public class Honda : ICarSupplier
    {
        public string CarColor => "Red";
        public void GetCarModel()
        {
            Console.WriteLine("Honda car model is Honda 2014.");
        }
    }
}