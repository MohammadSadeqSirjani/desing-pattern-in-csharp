﻿using System;

namespace FactoryMethod.Exercise
{
    public class Toyota : ICarSupplier
    {
        public string CarColor => "Yellow";
        public void GetCarModel()
        {
            Console.WriteLine("Toyota car model is Honda 2019.");
        }
    }
}