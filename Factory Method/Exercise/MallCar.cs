﻿using System.Collections.Generic;

namespace FactoryMethod.Exercise
{
    public class MallCar : IShop
    {
        public IList<ICarSupplier> PurchaseCar()
        {
            return new List<ICarSupplier>()
            {
                new Benz(),
                new Bmw(),
            };
        }
    }
}