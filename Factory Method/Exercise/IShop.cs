﻿using System.Collections.Generic;

namespace FactoryMethod.Exercise
{
    public interface IShop
    {
        public IList<ICarSupplier> PurchaseCar();
    }
}
