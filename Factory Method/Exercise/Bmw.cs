﻿using System;

namespace FactoryMethod.Exercise
{
    public class Bmw : ICarSupplier
    {
        public string CarColor => "White";
        public void GetCarModel()
        {
            Console.WriteLine("Bmw car model is Honda 2008.");
        }
    }
}